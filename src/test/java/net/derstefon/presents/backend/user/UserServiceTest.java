package net.derstefon.presents.backend.user;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Example;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createUser_notExisting() {
        when(userRepository.exists(ArgumentMatchers.any(Example.class))).thenReturn(false);

        User newUser = new User();
        newUser.setName("Test");
        newUser.setEmailAddress("Test@test.at");
        User result = userService.createUser(newUser);

        assertEquals(result.getName(), "Test");
        assertEquals(result.getEmailAddress(), "Test@test.at");
        assertNotNull(result.getId());
    }

    @Test()
    void createUser_existing() {
        when(userRepository.exists(ArgumentMatchers.any(Example.class))).thenReturn(true);

        assertThrows(
                IllegalArgumentException.class,
                () -> userService.createUser(new User())
        );
    }

}