package net.derstefon.presents.backend.present;

public class Present {

    private String id;

    private String name;

    public Present() {

    }

    public Present(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Present{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
