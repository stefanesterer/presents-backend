package net.derstefon.presents.backend.present;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class PresentController {

    @Autowired
    private PresentService presentService;

    @GetMapping("present/{userId}")
    public Collection<Present> getPresents(@PathVariable String userId) {
        return presentService.getPresents(userId);
    }

    @PostMapping("/present/{userId}")
    public @ResponseBody
    ResponseEntity<Present> addPresent(@PathVariable String userId, @RequestBody Present present) {
        presentService.addPresent(userId, present);
        return new ResponseEntity<>(present, HttpStatus.OK);
    }

    @DeleteMapping("present/{userId}/{presentId}")
    public void deletePresent(@PathVariable String userId, @PathVariable String presentId)
    {
        presentService.deletePresent(userId, presentId);
    }

}
