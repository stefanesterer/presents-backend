package net.derstefon.presents.backend.present;

import net.derstefon.presents.backend.user.User;
import net.derstefon.presents.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
class PresentService {

    @Autowired
    private UserService userService;

    void addPresent(String userId, Present present) {
        present.setId(UUID.randomUUID().toString());
        userService.addPresent(userId, present);
    }

    Collection<Present> getPresents(String userId) {
        return Optional.of(userService.getUser(userId))
                .map(User::getPresents)
                .orElseGet(ArrayList::new);
    }

    public void deletePresent(String userId, String presentId) {
        User user = userService.getUser(userId);
        user.getPresents().stream()
                .filter(present -> present.getId().equals(presentId))
                .findFirst()
                .ifPresent(
                        present -> user.getPresents().remove(present)
                );
        userService.saveUser(user);
    }
}
