package net.derstefon.presents.backend.user;

import net.derstefon.presents.backend.present.Present;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    User createUser(User user) {

        if (userExists(user.getName())) {
            throw new IllegalArgumentException("User already exists");
        }

        user.setId(UUID.randomUUID().toString());

        userRepository.save(user);
        return user;
    }

    private boolean userExists(String name) {

        Example<User> example = Example.of(new User());
        example.getProbe().setName(name);
        return userRepository.exists(example);
    }

    public void addPresent(String userId, Present present) {
        userRepository.findById(userId)
                .ifPresent(user -> {
                    user.getPresents().add(present);
                    userRepository.save(user);
                });
    }

    public User getUser(String id) {
        return userRepository.findById(id).orElse(null);
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }
}
