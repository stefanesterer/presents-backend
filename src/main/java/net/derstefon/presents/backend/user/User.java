package net.derstefon.presents.backend.user;

import net.derstefon.presents.backend.event.Event;
import net.derstefon.presents.backend.present.Present;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;

@Document
public class User {

    @Id
    private String id;

    private String name;
    private String emailAddress;
    private Collection<Present> presents = new ArrayList<>();
    private Collection<Event> events = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<Present> getPresents() {
        return presents;
    }

    public Collection<Event> getEvents() {
        return events;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", presents=" + presents +
                ", events=" + events +
                '}';
    }
}


