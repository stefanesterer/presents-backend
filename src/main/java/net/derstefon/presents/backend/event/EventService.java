package net.derstefon.presents.backend.event;

import net.derstefon.presents.backend.user.User;
import net.derstefon.presents.backend.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class EventService {

    @Autowired
    private UserService userService;

    public void create(String userId, Event event) {
        event.setId(UUID.randomUUID().toString());
        User user = userService.getUser(userId);
        user.getEvents().add(event);
        userService.saveUser(user);
    }

    public User addPresent(String eventId, String userId, String presentId) {

        User user = userService.getUser(userId);
        user.getEvents().stream().filter(event -> event.getId().equals(eventId))
                .findFirst()
                .ifPresent(event -> movePresent(user, event, presentId));
        return user;
    }

    private void movePresent(User user, Event event, String presentId) {
        user.getPresents().stream()
                .filter(present -> present.getId().equals(presentId))
                .findFirst()
                .ifPresent(present -> {
                    event.getPresents().add(present);
                    user.getPresents().remove(present);
                    userService.saveUser(user);
                });
    }
}
