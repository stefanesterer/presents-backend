package net.derstefon.presents.backend.event;

import net.derstefon.presents.backend.present.Present;

import java.util.ArrayList;
import java.util.Collection;

public class Event {

    private String id;
    private String name;
    private Collection<Present> presents = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Present> getPresents() {
        return presents;
    }

    public void setPresents(Collection<Present> presents) {
        this.presents = presents;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", presents=" + presents +
                '}';
    }
}
