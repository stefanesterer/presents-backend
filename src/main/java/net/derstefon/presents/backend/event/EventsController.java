package net.derstefon.presents.backend.event;

import net.derstefon.presents.backend.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class EventsController {

    @Autowired
    private EventService eventService;

    @PostMapping("event/{userId}")
    public @ResponseBody
    ResponseEntity<Event> create(@PathVariable String userId, @RequestBody Event event) {
        eventService.create(userId, event);
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @PostMapping("event/{eventId}/{userId}/{presentId}")
    public @ResponseBody
    ResponseEntity<User> addPresent(@PathVariable String eventId, @PathVariable String userId, @PathVariable String presentId) {
        return new ResponseEntity<>(eventService.addPresent(eventId, userId, presentId), HttpStatus.OK);
    }

}
