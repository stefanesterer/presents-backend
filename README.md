# Usage:

## Local Development

* Start mongo db: docker run -p 27017:27017 --name presents-db -d mongo:latest
* Start presents-backend in desired ide
* investigate local mongo-db with: docker run --link presents-db:mongo -p 8081:8081 mongo-express:latest

## Deploy

* docker run -p 27017:27017 --name presents-db -d mongo:latest (TODO: make it not accesible from outside, persist DB in filesystem, ...)
* docker run --name presents-backend -d -p 8083:8080 -e spring.data.mongodb.host=mongo --link presents-db:mongo registry.gitlab.com/stefanesterer/presents-backend:latest

## Interact via rest

* create user: http POST localhost:8080/user name=TestUser
* create present: http POST localhost:8080/present/{userId} name=Present_1
* get presents for user: http get localhost:8080/present/{userId}
* get all existing users: http get localhost:8080/user
* create event: http post localhost:8080/event/{userId} name=Event_1
* move present to a specific event: http post localhost:8080/event/{eventId}/{userId}/{presentId} 

# Todo: